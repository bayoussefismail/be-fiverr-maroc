<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\GigController;
use App\Http\Controllers\ProfileController;

//get auth user
Route::middleware('auth:api')->get('/user', [UserController::class, 'getAuthenticatedUser']);

//gigs routes
Route::get('gigs', [GigController::class, 'index']);
Route::get('gigs/{id}', [GigController::class, 'show']);
Route::put('gigs/{id}', [GigController::class, 'edit']);

Route::middleware('auth:api')->post('/gigs', [GigController::class, 'store']);
Route::middleware('auth:api')->post('/profile', [ProfileController::class, 'store']);
//auth routes
Route::post('login', [UserController::class, 'login']);
Route::post('signup', [UserController::class, 'signup']);
