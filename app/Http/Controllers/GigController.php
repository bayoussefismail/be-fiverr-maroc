<?php

namespace App\Http\Controllers;

use App\Models\Gig;
use App\Http\Requests\StoreGigRequest;
use App\Http\Requests\UpdateGigRequest;
use App\Http\Resources\GigCollection;

class GigController extends Controller
{

    public function index()
    {
        $gigs = Gig::all();
        return GigCollection::collection($gigs);
    }

    public function store(StoreGigRequest $request)
    {
        $gig = new Gig();
        $gig->title = $request->title;
        $gig->price = $request->price;
        $gig->description = $request->description;
        $gig->delivery_time = $request->delivery_time;
        $gig->pictures = $request->pictures;
        $gig->user_id = $request->user()->id;
        $gig->save();

        return response()->json(["gig" => $gig, "user" => $request->user()], 201);
    }


    public function show($id)
    {
        $gig = Gig::findOrFail($id);
        return new GigCollection($gig);
    }

    public function update(UpdateGigRequest $request, $id)
    {
        $gig = Gig::findOrFail($id);
        $gig->update($request->all());
        return new GigCollection($gig);
    }

    public function destroy($id)
    {
        $gig = Gig::findOrFail($id);
        $gig->delete();
        return response()->json(null, 204);
    }
}
