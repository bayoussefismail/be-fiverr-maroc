<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Exception;
use App\models\User;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Str;

class UserController extends Controller
{
    public function getAuthenticatedUser(Request $request)
    {
        $gigs = $request->user()->gigs;
        try {
            $profile = $request->user()->profile;
        } catch (Exception $e) {
            return $e;
        }
        return response()->json([$request->user()]);
    }
    public function login(Request $request)
    {
        $creds = $request->only(['email', 'password']);

        $token = auth()->attempt($creds);
        if (!$token = auth()->attempt($creds)) {
            return response()->json(['error' => 'incorrect email/password'], 401);
        }
        return response()->json(['token' => $token]);
    }
    public function signup(Request $request)
    {

        try {
            $validatedData = $request->validate([
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:6',
            ]);
        } catch (ValidationException $e) {
            return response()->json([
                'error' => $e->getMessage(),
                'errors' => $e->errors(),
            ], 422);
        }


        // Create the user
        $user = User::create([
            'name' => $validatedData['name'],
            'email' => $validatedData['email'],
            'password' => Hash::make($validatedData['password']),
        ]);



        // Return the token and the user information
        return response()->json([
            'user' => $user,
        ], 201);
    }
}
