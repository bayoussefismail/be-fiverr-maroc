<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index($id)
    {
        $profiles = Profile::all();
        return $profiles;
    }

    public function store(Request $request)
    {
        $profile = new Profile();
        $profile->job_title = $request->job_title;
        $profile->profile_picture = $request->profile_picture;
        $profile->country = $request->country;
        $profile->badges = $request->badges;
        $profile->languages = $request->languages;
        $profile->skills = $request->skills;
        $profile->education = $request->education;
        $profile->experience = $request->experience;
        $profile->socials = $request->socials;
        $profile->description = $request->description;
        $profile->portfolio = $request->portfolio;
        $profile->address = $request->address;
        $profile->user_id = $request->user()->id;
        $profile->save();

        return response()->json([$profile], 201);
    }


    public function show($id)
    {
        $profile = Profile::findOrFail($id);
        return $profile;
    }

    public function update(Request $request, $id)
    {
        $profile = Profile::findOrFail($id);
        $profile->update($request->all());
        return $profile;
    }
}
