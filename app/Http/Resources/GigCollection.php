<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class GigCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $user = User::findOrFail($this->user_id);
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            "delivery_time" => $this->delivery_time,
            "pictures" => $this->pictures,
            'user' => $user,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
