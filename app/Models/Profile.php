<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;

class Profile extends Model
{

    use HasFactory;
    use Uuid;
    protected $fillable = [
        "job_title",
        'profile_picture',
        'badges',
        'country',
        'languages',
        'skills',
        'education',
        'experience',
        'socials',
        'description',
        'portfolio',
        'address',
    ];
    protected $casts = [
        'badges' => 'array',
        'languages' => 'array',
        'skills' => 'array',
        'education' => 'array',
        'experience' => 'array',
        'socials' => 'array',
        'portfolio' => 'array',
    ];
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
