<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;

class Gig extends Model
{
    use Uuid;
    use HasFactory;

    protected $fillable = [
        "id",
        'title',
        'price',
        'description',
        'delivery_time',
        'pictures',
        'user_id'
    ];

    protected $casts = [
        'pictures' => 'array',
    ];
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
